import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { Toast } from '@ionic-native/toast';
import { NativeAudio } from '@ionic-native/native-audio';

import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { BrowsePage } from '../pages/browse/browse';
import { SearchPage } from '../pages/search/search';
import { RadioPage } from '../pages/radio/radio';
import { LibraryPage } from '../pages/library/library';
import { LoginPage } from '../pages/login/login';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpProvider } from '../providers/http/http';

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    HomePage,
    LoginPage,
    RadioPage,
    BrowsePage,
    SearchPage,
    LibraryPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, { tabsHideOnSubPages: true })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    HomePage,
    RadioPage,
    LoginPage,
    BrowsePage,
    SearchPage,
    LibraryPage
  ],
  providers: [
    Toast,
    StatusBar,
    NativeAudio,
    HttpProvider,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
