import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class HttpProvider {

    private url: string = "./assets/mocks/user.json"
    // private url: string = "http://138.68.175.66/mocks/user.json";


    constructor(public http: HttpClient) {
        console.log('Hello HttpProvider Provider');
    }

    getUser() {
        return this.http.get(this.url)
    }

}