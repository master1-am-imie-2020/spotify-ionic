export class Song {
    name: string;
    artist: string;
    url: string;
    cover: string;
    time: string;
}

export class Playlist {
    name: string;
    genre: string;
    cover: string;
    songs: Song[];
}

export class User {
    name: string;
    avatar: string = "https://cdn.pixabay.com/photo/2016/09/22/16/38/avatar-1687700_960_720.jpg";
    email: string;
    follower: number;
    following: number;
    playlists: Playlist[];
    history: Song[];
}