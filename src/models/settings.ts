export class Settings{
    offlineMode: boolean = false;
    rangeSwitchSong: number = 0;
    gapless: boolean = false;
    unplayableSongs: boolean = false;
    normalizeVolume: boolean = false;
    broadcastStatus: boolean = false;
    autoplay: boolean = false;
    lyrics: boolean = false;
    strangerThings: boolean = false;
    devicesOnly: boolean = false;
    privateSession: boolean = false;
    lastFm: boolean = false;
    navigation: boolean = false;
    quality: string = "extreme";
}