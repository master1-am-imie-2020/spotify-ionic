import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, LoadingController } from 'ionic-angular';
import { User } from '../../models/user';

@IonicPage()
@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html',
})
export class ProfilePage {
    section: string = 'two';
    somethings: any = new Array(20);
    private user: User = new User()

    constructor(public navCtrl: NavController, private viewCtrl: ViewController, private loadingCtrl: LoadingController) {
        let loading = this.loadingCtrl.create({
            spinner: "bubbles",
            content: "Chargement..."
        });
        loading.present();
        let user = JSON.parse(localStorage.getItem("user"))
        this.user = {
            name: user.name,
            avatar: user.avatar,
            email: user.email,
            follower: user.follower,
            following: user.following,
            playlists: user.playlists,
            history: [user.song]
        }
        loading.dismiss();
    }

    dismiss() {
        this.viewCtrl.dismiss();
    };
}