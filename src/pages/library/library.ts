import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../models/user';

@IonicPage()
@Component({
    selector: 'page-library',
    templateUrl: 'library.html',
})
export class LibraryPage {

    private user: User = new User()
    constructor(public navCtrl: NavController, public navParams: NavParams) {}

    ionViewDidLoad() {
        let user = JSON.parse(localStorage.getItem("user"))
        this.user = {
            name: user.name,
            avatar: user.avatar,
            email: user.email,
            follower: user.follower,
            following: user.following,
            playlists: user.playlists,
            history: [user.song]
        }
        console.log('ionViewDidLoad LibraryPage');
    };

    goToSettings() {
        this.navCtrl.push('SettingsPage');
    };

    showMusicPlayer() {
        this.navCtrl.push('MusicPlayerPage');
    };

    goToProfile() {
        this.navCtrl.push('ProfilePage');
    }
}