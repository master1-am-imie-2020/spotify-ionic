import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, LoadingController, ViewController } from 'ionic-angular';
import { LoginPage } from '../login/login';

import { Settings } from '../../models/settings';

@IonicPage()
@Component({
    selector: 'page-settings',
    templateUrl: 'settings.html',
})
export class SettingsPage {

    private settings: Settings = new Settings()

    constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private app: App,
        private loadingCtrl: LoadingController) {}

    ionViewDidLoad() {
        let param = JSON.parse(localStorage.getItem("settings"))
        this.settings = (param === undefined || param === null || param == "") ? this.settings : param
        console.log('ionViewDidLoad SettingsPage');
    };

    dismiss() {
        localStorage.setItem("settings", JSON.stringify(this.settings))
        this.viewCtrl.dismiss();
    };

    signOut() {
        var loading = this.loadingCtrl.create({
            spinner: "bubbles",
            content: "Logging out..."
        });

        loading.present();

        setTimeout(() => {
            loading.dismiss();
            localStorage.removeItem("user");
            localStorage.removeItem("settings");
            this.app.getRootNav().setRoot(LoginPage);
        }, 2000);
    }
}