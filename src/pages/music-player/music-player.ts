import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';

import { NativeAudio } from '@ionic-native/native-audio';

@IonicPage()
@Component({
    selector: 'page-music-player',
    templateUrl: 'music-player.html',
})
export class MusicPlayerPage {
    constructor(private viewCtrl: ViewController, private audio: NativeAudio) {
    }

    ionViewDidLoad() {
        this.audio.preloadSimple('uniqueId1', 'http://138.68.175.66/audio/sound1.mp3').then((data) => console.log(data), (data) => console.info(data))
        console.log('ionViewDidLoad MusicPlayerPage');
    };

    ionViewDidLeave() {
        console.log('ionViewDidLeave MusicPlayerPage');
    };

    close() {
        this.viewCtrl.dismiss();
    };

    play(){
        this.audio.play("uniqueId1", () => console.log('uniqueId1 is done playing'));
    }

    stop() {
        this.audio.stop("uniqueId1");
    }
}