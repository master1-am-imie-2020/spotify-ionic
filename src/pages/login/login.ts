import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { Toast } from '@ionic-native/toast';

import { TabsPage } from '../tabs/tabs';
import { HttpProvider } from '../../providers/http/http';

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {
    constructor(public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController, private api: HttpProvider, private toast: Toast) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad LoginPage');
    }

    goToTabs() {
        var loading = this.loadingCtrl.create({
            spinner: "bubbles",
            content: "Connexion en cours..."
        });

        loading.present();

        setTimeout(() => {
            this.api.getUser().subscribe((data:any) => {
                console.log(data)
                this.toast.show("Bienvenuz M." + data.name, '3000', 'center')
                localStorage.setItem("user", JSON.stringify(data));
                loading.dismiss();
                this.navCtrl.setRoot(TabsPage);
            })
        }, 2000);
    };
}