import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlaylistPage } from './playlist';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
    declarations: [
        PlaylistPage
    ],
    imports: [
        IonicPageModule.forChild(PlaylistPage),
        ComponentsModule
    ],
})
export class PlaylistPageModule {}