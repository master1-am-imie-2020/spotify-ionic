import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-playlist',
    templateUrl: 'playlist.html',
})
export class PlaylistPage {
    constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController) {}

    ionViewDidLoad() {
        console.log('ionViewDidLoad PlaylistPage');
    };

    dismiss() {
        this.viewCtrl.dismiss();
    };
}