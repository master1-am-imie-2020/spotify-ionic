### Project Spotify for Ionic 3
This name is available on the catalog for a easy-to-buy-to-one via the partner merchant website4. The program / service, in its free version, is only available in Western Europe while the version is beta, but it is possible in almost all countries.

### How to run ?
You need npm, cordova and ionic installed
* 1- Download or clone this repo
    git clone https://gitlab.com/master1-am-imie-2020/spotify-ionic.git spotify
* 2- In folder of project run
 ```shell
 npm i
  ```
* 3 Run ionic 
 ```shell
 ionic serve --lab
  ```
  The new tab in your favorite navigator will open
  
* 4 Run to the device android 
 ```shell
 ionic cordova build android
  ```
  go to the folder ./platforms/android/app/build/outputs/apk/debug/ and share the file app-debug.apk
  
 * 5 Enjoy


### Environment used in development

 ```shell
(_) ___  _ __ (_) ___
| |/ _ \| '_ \| |/ __|
| | (_) | | | | | (__
|_|\___/|_| |_|_|\___|  CLI 4.5.0

cli packages: (/usr/lib/node_modules)

    @ionic/cli-utils  : 1.9.2
    ionic (Ionic CLI) : 4.5.0

local packages:

    @ionic/app-scripts : 3.1.8
    Ionic Framework    : ionic-angular 3.9.2

System:

    Node : v10.8.0
    npm  : 6.4.1 
    OS   : Mac OS
 ```
